# coding: utf-8
'''
Félix NESTI Annah GEDEON Isabelle AMARA Oskar MOREAU
Ce programme permet de déterminer la maison d'un personnage donné dans Harry Potter.
https://gitlab.com/nsi_felix_nesti/projet_choipeau_magique
v 5
25/01/2024
'''

import csv
from math import sqrt

#--------------------------------------------------------------------------------------------------

profils = [{'Name' : 'profil1', 'Courage' : 9, 'Ambition' : 2, 'Intelligence' : 8, 'Good' : 9}, 
            {'Name' : 'profil2', 'Courage' : 6, 'Ambition' : 7, 'Intelligence' : 9, 'Good' : 7}, 
            {'Name' : 'profil3', 'Courage' : 3, 'Ambition' : 8, 'Intelligence' : 6, 'Good' : 3}, 
            {'Name' : 'profil4', 'Courage' : 2, 'Ambition' : 3, 'Intelligence' : 7, 'Good' : 8}, 
            {'Name' : 'profil5', 'Courage' : 3, 'Ambition' : 4, 'Intelligence' : 8, 'Good' : 8}]

char_manual = {'Courage' : '', 'Ambition' : '', 'Intelligence' : '', 'Good' : ''}

nbrs = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']

k = 5

#--------------------------------------------------------------------------------------------------

def csv_opening():
    """
    Ouvre les fichiers csv et les fusionne.

    Entrée : none
    Sortie : table des personnages fusionnée
    """
    with open("Characters.csv", mode='r', encoding='utf-8') as f:
        reader = csv.DictReader(f, delimiter=';')
        characters_tab = [dico for dico in reader]

    with open("Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
        reader = csv.DictReader(f, delimiter=';')
        characteristics_tab = [dico for dico in reader]

    for kaggle_character in characters_tab:
        for poudlard_character in characteristics_tab:
            if poudlard_character['Name'] == kaggle_character['Name']:
                kaggle_character.update(poudlard_character)
    
    return characters_tab

#--------------------------------------------------------------------------------------------------

def distance(perso1, perso2, methode='euclidienne'):
    """
    Calcule la distance entre deux personnages selon leurs caractéristiques.

    Entrée : dictionnaires des personnages
    Sortie : distance en float
    """
    return sqrt(((int(perso1['Courage'])) - int(perso2['Courage'])) ** 2 + 
    (int(perso1['Ambition']) - int(perso2['Ambition'])) ** 2 + 
    (int(perso1['Intelligence']) - int(perso2['Intelligence'])) ** 2 + 
    (int(perso1['Good']) - int(perso2['Good'])) ** 2)

#--------------------------------------------------------------------------------------------------

def ppv(tab, perso):
    """
    Classer les personnages de la table selon leur distance avec le personnage cible
    afin de détermnier les plus proches voisins.

    Entrée : table des personnages avec leurs caractéristiques, caractéristiques du personnage cible
    Sortie : liste des personnages classée selon leur distance
    """
    ppvs = []
    for elt in tab:
        if 'Courage' in elt:
            voisin = [elt['Name'], distance(elt, perso), elt['House']]
            ppvs.append(voisin)
    ppvs.sort(key=lambda x:x[1])
    return ppvs

#--------------------------------------------------------------------------------------------------

def maison(table):
    """
    Détermine la maison qui correspond au personnage cible en fonction des plus proches voisins.

    Entrée : liste des plus proches voisins
    Sortie : maison correspondant au personnage
    """
    maisons = {'Gryffindor' : 0, 'Hufflepuff' : 0, 'Ravenclaw' : 0, 'Slytherin' : 0}

    for elt in table:
        maisons[elt[2]] += 1
    classement_maisons = list(maisons.items())
    classement_maisons.sort(key=lambda x:x[1], reverse=True)

    return classement_maisons[0][0]

#--------------------------------------------------------------------------------------------------

def affichage(voisins):
    """
    Réalise l'affichage des résulats du choix de la maison pour le personnage ainsi que ses voisins.

    Entrée : none
    Sortie : none
    """
    for i in range(5):
        print(f'La maison du profil {i+1} est : ', maison(voisins[i]))
        print('\nSes voisins sont : ', '\n')
        for voisin in voisins[i]:
            print(voisin[0], '\nDans la maison : ', voisin[2], '\n')
        print('-----------------------------------------------\n')

#--------------------------------------------------------------------------------------------------

def perso_manuel(char, nbrs):
    for elt in char:
        while char[elt] not in nbrs:
            char[elt] = input(f'{elt} : ')
        char[elt] = int(char[elt])
    return char

def affichage_manuel(neigh):
    print('\nLa maison qui vous correspond est : ', maison(neigh))
    print('\nVos voisins sont : ', '\n')
    for voisin in neigh:
        print(voisin[0], '\nDans la maison : ', voisin[2], '\n')     

#--------------------------------------------------------------------------------------------------

kppvs = [ppv(csv_opening(), profils[i])[:k] for i in range(5)]
affichage(kppvs)
print('Il vous est maintenant possible de rentrer vos propres caractéristiques\n' 
    'afin de vous associer à la maison qui vous correspond :\n')
kppvs_manual = ppv(csv_opening(), perso_manuel(char_manual, nbrs))[:k]
affichage_manuel(kppvs_manual)
